---
title: Bonnes pratiques
description: ""
date: 2022-08-28T20:17:47.115Z
preview: ""
draft: oui
tags:
  - Git
  - Workflow
  - Flux de développement
  - Bonnes pratiques
categories: ""
lastmod: 2022-08-28T20:43:09.888Z
---
Pour créer ce blog, j'ai utilisé et suivi plusieurs bonnes pratiques et plusieurs blog / sites que je vais vous présenter.
(Enfin j'essaye de les suivres à chaques fois ;))

**Bonnes pratiques :**
 - Ne pas pousser sur la branche principale
 - Utiliser les Merge Requests (MR)


**Liens :**
 - Publier un BLOG GRATUITEMENT avec HUGO, GitLab Page et GitLab CI en MOINS D’UNE HEURE ! | TUTO GitLab-CI

https://lydra.fr/publier-un-blog-gratuitement-avec-hugo-gitlab-page-et-gitlab-ci-en-moins-dune-heure-tuto-gitlab-ci/


 - TUTO | GitLab | Comment configurer et sécuriser son dépôt GitLab ?

https://lydra.fr/comment-configurer-et-securiser-son-depot-gitlab/


 - Bonnes pratiques | Git | Quel flux de développement git choisir ?

https://lydra.fr/bonnes-pratiques-git-quel-flux-de-developpement-git-choisir/
